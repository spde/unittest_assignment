package com.sushmitha.junit.junit_programs;

public class Add2
{
	
	
	public int add(int num1,int num2)
	{
	    return num1+num2;
	}

	 @FunctionalInterface
	 
	 interface Addition {
     int operation(int number1, int number2);
		 
	 
	 }Addition additionOp = (number1, number2) -> number1 + number2;
	 }

