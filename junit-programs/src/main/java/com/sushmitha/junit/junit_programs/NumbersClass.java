package com.sushmitha.junit.junit_programs;

import java.util.ArrayList;


	 public class NumbersClass
	 {
	    ArrayList<Integer> evenNumbersArrayListA1 = new ArrayList<>();
	    ArrayList<Integer> evenNumbersArrayListA1MultipliedBy2ThenA2 = new ArrayList<>();
	    String string = new String("");


	    public ArrayList<Integer> saveEvenNumbers(int N)
	    {
	        for (int i=2;i<=N;i++){
	            if(i%2==0)
	                evenNumbersArrayListA1.add(i);
	        }

	        return evenNumbersArrayListA1;
	    }

	    public ArrayList<Integer> printEvenNumbers(){
	        for (int i:evenNumbersArrayListA1){
	            evenNumbersArrayListA1MultipliedBy2ThenA2.add(i*2);
	        }

	        for (int i:evenNumbersArrayListA1MultipliedBy2ThenA2){
	            string+=i + ",";
	        }
	        System.out.println(string);

	        return evenNumbersArrayListA1MultipliedBy2ThenA2;
	    }

	    public int printEvenNumber(int N){
	        for (int i:evenNumbersArrayListA1){
	            if (i==N)
	                return i;
	        }

	        return 0;
	    }
	 }


