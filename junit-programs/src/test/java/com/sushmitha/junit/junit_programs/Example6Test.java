package com.sushmitha.junit.junit_programs;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import org.junit.jupiter.api.Test;
		



		class Example6Test {
		    Example6 que = new Example6();



		   @Test
		    void testSaveEvenNumbers() {
		        List<Integer> expectedList = Arrays.asList(2, 4, 6, 8, 10);
		        assertArrayEquals(expectedList.toArray(), que.saveEvenNumbers(10).toArray());



		   }



		   @Test
		    void testPrintEvenNumbers() {
		        ArrayList<Integer> inputList = que.saveEvenNumbers(10);
		        List<Integer> resultList = Arrays.asList(4, 8, 12, 16, 20);
		        assertArrayEquals(resultList.toArray(), que.printEvenNumbers(inputList).toArray());
		    }
		
	}


