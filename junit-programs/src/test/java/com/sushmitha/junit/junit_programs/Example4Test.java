package com.sushmitha.junit.junit_programs;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class Example4Test
{

	public static void main(String args[])
	{
		System.out.println(Thread.currentThread().getName()+" start");
		Example4 t1= new Example4();
		t1.setName("T1");
		t1.setDaemon(true);
		t1.start();
		System.out.println(Thread.currentThread().getName()+" End");
	}
	

}
