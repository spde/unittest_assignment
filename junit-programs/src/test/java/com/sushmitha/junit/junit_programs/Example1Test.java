
package com.sushmitha.junit.junit_programs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

class Example1Test {


	@Test
	 void nonStaticTest() {
        Example1 q1 = new Example1();
        Example1 q2 = new Example1();
        Example1 q3 = new Example1();
        System.out.println(q1.countNonStatic);
        assertNotEquals(3, q1.countNonStatic);
   }	   
	@Test
	void staticTest() {
		Example1 p1 = new Example1();
	    Example1 p2 = new Example1();
	    Example1 p3 = new Example1();
	    System.out.println(p3.countStatic);
	    assertEquals(3, p3.countStatic);
	}
}


		   
		   


