package com.sushmitha.junit.junit_programs;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Add2Test {

	
		Add2 Add2 = new Add2();
	    @Test
	    
	   
	    void testAdd2()
	    {
	        assertEquals(14,Add2.add(8,6));
	    }
	    
	    @Test
	    @DisplayName("Addition using Lamda expression")
	    void testAddtionUsingLambda()
	    {
	        assertEquals(10, Add2.additionOp.operation(1, 9));
	    }
	}


