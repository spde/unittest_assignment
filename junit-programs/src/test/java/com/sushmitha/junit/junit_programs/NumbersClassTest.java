package com.sushmitha.junit.junit_programs;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;



		import java.util.ArrayList;
		import java.util.Arrays;

		public class NumbersClassTest {

		   @Test
		   public void shouldReturnListOfEvenNumbersWhenNIsGiven(){
		       NumbersClass numbersClassObj = new NumbersClass();
		       ArrayList<Integer> expectedEvenNumbersArrayListA1 = new ArrayList<>(Arrays.asList(2,4,6,8,10));

		       ArrayList<Integer> actualEvenNumbersArrayListA1 = numbersClassObj.saveEvenNumbers(10);

		       Assertions.assertEquals(expectedEvenNumbersArrayListA1,actualEvenNumbersArrayListA1);
		   }

		   @Test
		   public void shouldReturnListOfEvenNumbersArrayListA1MultipliedBy2ThenA2(){
		       NumbersClass numbersClassObj = new NumbersClass();
		       numbersClassObj.evenNumbersArrayListA1.add(2);
		       numbersClassObj.evenNumbersArrayListA1.add(4);
		       numbersClassObj.evenNumbersArrayListA1.add(6);
		       numbersClassObj.evenNumbersArrayListA1.add(8);
		       numbersClassObj.evenNumbersArrayListA1.add(10);
		       ArrayList<Integer> expectedEvenNumbersArrayListA1MultipliedBy2ThenA2 = new ArrayList<>(Arrays.asList(4,8,12,16,20));

		       ArrayList<Integer> actualEvenNumbersArrayListA1MultipliedBy2ThenA2 = numbersClassObj.printEvenNumbers();

		       Assertions.assertEquals(expectedEvenNumbersArrayListA1MultipliedBy2ThenA2,actualEvenNumbersArrayListA1MultipliedBy2ThenA2);
		   }

		   @Test
		   public void shouldReturnIntegerPassedIfExistsInEvenNumbersArrayListA1(){
		       NumbersClass numbersClassObj = new NumbersClass();
		       numbersClassObj.evenNumbersArrayListA1.add(2);
		       numbersClassObj.evenNumbersArrayListA1.add(4);
		       numbersClassObj.evenNumbersArrayListA1.add(6);
		       numbersClassObj.evenNumbersArrayListA1.add(8);
		       numbersClassObj.evenNumbersArrayListA1.add(10);

		       int i = numbersClassObj.printEvenNumber(8);

		       Assertions.assertEquals(8,i);
		   }
		

	}


