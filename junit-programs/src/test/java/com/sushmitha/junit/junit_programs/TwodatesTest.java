package com.sushmitha.junit.junit_programs;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;



public class TwodatesTest {

	@Test
	public void shouldReturndifferenceBetweenDatesWhenDatesAreGiven()
	{
		Twodates main = new Twodates();
		LocalDate localDate1 = LocalDate.of(2022,12,31);
		LocalDate localDate2 = LocalDate.of(2021,11,30);
		LocalDate ExpectedLocalDate = LocalDate.of(1,1,1);
		LocalDate ActualLocalDate = main.diiferenceBetweenDates(localDate1,localDate2);
		Assertions.assertEquals(ExpectedLocalDate,ActualLocalDate);
		
		
	}

}
