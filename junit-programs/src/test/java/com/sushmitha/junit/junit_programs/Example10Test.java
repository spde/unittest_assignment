package com.sushmitha.junit.junit_programs;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

     class Example10Test {



		   @Test
		    void test1duplicates() {
		        String input="hello";
		        int result=Example10.countofDuplicates(input);
		        assertNotNull(input);
		        assertEquals(1,result);
		       
		    }
		    
		    @Test
		    void test2duplicates() {
		        String input="helllo";
		        int result=Example10.countofDuplicates(input);
		        assertNotNull(input);
		        assertEquals(2,result);
		        
		    }
		    
		    @Test
		    void test3duplicates() {
		        String input="hellloo";
		        int result=Example10.countofDuplicates(input);
		        assertNotNull(input);
		        assertEquals(3,result);
		       
		    }
		    
		   
     }




		
	


