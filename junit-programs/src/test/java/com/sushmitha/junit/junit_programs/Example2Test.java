package com.sushmitha.junit.junit_programs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import org.junit.jupiter.api.Test;



class Example2Test {
	@Test
	void testDigit() {
		assertEquals("Digit", Example2.checkCharacter('2'));
	}

	

	@Test
	void testUpper1() {

		char ch = 'A';
		assertEquals("UpperCase", Example2.checkCharacter(ch));
	}

	@Test
	void testUpper2() {
		char ch = 'b';
		assertNotEquals("UpperCase", Example2.checkCharacter(ch));

	}

	@Test
	void testLower() {

		char ch = 'a';
		assertEquals("LowerCase", Example2.checkCharacter(ch));
	}
}